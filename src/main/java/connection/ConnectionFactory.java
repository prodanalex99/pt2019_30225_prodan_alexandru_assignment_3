package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConnectionFactory {
	/***
	 * Variabilele instanta LOGGER,DRIVER,DRURL,USER,PASS si instance prin intermediul carora se realizeaza conexiunea la baza de date
	 * ***/
	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/WareHouse";
	private static final String USER = "root";
	private static final String PASS = "maralbastru15";
	private static ConnectionFactory instance = new ConnectionFactory();
	/****
	 * constructorul privat al clasei
	 * ****/
	private ConnectionFactory()
	{
		try{
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	/****
	 * metoda de creare a unei noi conexiuni
	 * ***/
	private Connection createConnection()
	{
		Connection connection = null;
		try{
			connection = DriverManager.getConnection(DBURL,USER,PASS);
		}catch(SQLException e)
		{
			LOGGER.log(Level.WARNING, "An error occured at connection to the database");
		}
		return connection;
	}
	/*****
	 * metoda ce returneaza Conexiunea curenta
	 * *****/
	public static Connection getConnection()
	{
		return instance.createConnection();
	}
	/****
	 * metoda de inchidere a conexiunii curente
	 * *****/
	public static void close(Connection con)
	{
		if(con != null)
		{
			try{
				con.close();
			}catch(SQLException e)
			{
				LOGGER.log(Level.WARNING,"An error ocurend at closing connection!");
			}
		}
	}
	/***
	 * metoda de inchidere a statementului dat ca si argument
	 * ***/
	public static void close(Statement statement)
	{
		if(statement != null)
		{
			try{
				statement.close();
			}catch(SQLException e)
			{
				LOGGER.log(Level.WARNING, "An error ocured while trying to close statement");
			}
		}
	}
	/****
	 * metoda de inchidere a resultSetului dat ca si argument
	 * *****/
	public static void close(ResultSet rs)
	{
		if(rs != null)
		{
			try{
				rs.close();
			}catch(SQLException e)
			{
				LOGGER.log(Level.WARNING,"An error occured while trying to close the resultSet");
			}
		}
	}
}
