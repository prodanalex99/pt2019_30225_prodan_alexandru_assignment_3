package model;

public class Product {
	/**
	 * variabilele instanta ce corespund coloanelor din tabelul Product
	 * ***/
	private int idProduct;
	private String nameOfProduct;
	private float price;
	private int quantity;
	/***
	 * constructorii clasei
	 * ***/
	public Product(int id,String nameOfProduct,float price,int quantity)
	{
		this.idProduct = id;
		this.nameOfProduct = nameOfProduct;
		this.price = price;
		this.quantity = quantity;
	}
	public Product(String nameOfProduct,float price,int quantity)
	{
		this.nameOfProduct = nameOfProduct;
		this.price = price;
		this.quantity = quantity;
	}
	public String getNameOfProduct() {
		return nameOfProduct;
	}
	public void setNameOfProduct(String nameOfProduct) {
		this.nameOfProduct = nameOfProduct;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/****
	 * metoda toString() ce da o reprezentare a obiectelor definite din aceasta clasa
	 * *****/
	public String toString()
	{
		return String.format("Product Name=%s Price=%f Quantity=%d", this.getNameOfProduct(),this.getPrice(),this.getQuantity());
	}
	public int getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	
}
