package model;

import java.util.ArrayList;

public class Bill {
	/**
	 * variabilele instanta ce corespund coloanelor din tabelul Bill
	 * ***/
	private int idBill;
	private ArrayList<Order> orders;
	private float totalOrdersPrice;
	private int client_Id;
	private Client client;
	
	/***
	 * constructorii clasei
	 * ***/
	public Bill(Client client,int billNumber,ArrayList<Order> orders,float totalOrdersPrice)
	{
		this.setClient(client);
		this.idBill = billNumber;
		this.orders = orders;
		this.totalOrdersPrice = totalOrdersPrice;
		this.client_Id = client.getId();
	}
	public Bill(Client client,ArrayList<Order> orders,float totalOrdersPrice)
	{
		this.setClient(client);
		this.orders = orders;
		this.totalOrdersPrice = totalOrdersPrice;
		this.client_Id = client.getId();
	}
	public Bill()
	{
		
	}
	
	/****
	 * metode gettere si settere pentru variabilele instanta
	 * ***/

	public ArrayList<Order> getOrders() {
		return orders;
	}

	public void setOrders(ArrayList<Order> orders) {
		this.orders = orders;
	}

	public float getTotalOrdersPrice() {
		return totalOrdersPrice;
	}

	public void setTotalOrdersPrice(float totalOrdersPrice) {
		this.totalOrdersPrice = totalOrdersPrice;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	/****
	 * metoda toString() ce da o reprezentare a obiectelor definite din aceasta clasa
	 * *****/
	public String toString()
	{
		return null;
	}
	public void setIdBill(int idBill) {
		this.idBill = idBill;
	}
	public int getClientId() {
		return client_Id;
	}
	public void setClientId(int clientId) {
		this.client_Id = clientId;
	}
	public int getIdBill() {
		// TODO Auto-generated method stub
		return this.idBill;
	}
	
}
