package model;

public class Client {
	/**
	 * variabilele instanta ce corespund coloanelor din tabelul Client
	 * ***/
	private int id;
	private String firstName;
	private String lastName;
	private String email; 
	private String address;
	private String iban;//iban prefix si 12 sau mai multe cifre
	/***
	 * constructorii clasei
	 * ***/
	public Client(int id,String firstName,String lastName,String email,String address,String iban)
	{
		this.setId(id);
		this.firstName = firstName;
		this.iban = iban;
		this.setLastName(lastName);
		this.email = email;
		this.address = address;
	}
	public Client(String firstName,String lastName,String email,String address,String iban)
	{
		this.firstName = firstName;
		this.iban = iban;
		this.setLastName(lastName);
		this.email = email;
		this.address = address;
	}
	public Client()
	{
		//this(0,"Popescu","Ion","xxxxxxxxxxxxxx");
	}
	/****
	 * metode gettere si settere pentru variabilele instanta
	 * ***/
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String name) {
		this.firstName = name;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	/****
	 * metoda toString() ce da o reprezentare a obiectelor definite din aceasta clasa
	 * *****/
	public String toString()
	{
		return null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
