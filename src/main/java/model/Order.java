package model;

import java.util.ArrayList;

public class Order {
	/**
	 * variabilele instanta ce corespund coloanelor din tabelul Order
	 * ***/
	private int idOrder;
	private ArrayList<Product> products;
	private float totalPrice;
	private int bill_idBill;
	
	/***
	 * constructorii clasei
	 * ***/
	public Order(int id,ArrayList<Product> products,float totalPrice)
	{
		this.idOrder = id;
		this.products = products;
		this.totalPrice = totalPrice;
	}
	public Order(int id,float totalPrice)
	{
		this.idOrder = id;
		this.totalPrice = totalPrice;
	}
	public Order()
	{
		
	}
	public Order(ArrayList<Product> products,float totalPrice)
	{
		this.products = products;
		this.totalPrice = totalPrice;
	}
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	public ArrayList<Product> getProducts() {
		return products;
	}
	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}
	
	
	/****
	 * metoda toString() ce da o reprezentare a obiectelor definite din aceasta clasa
	 * *****/
	public String toString()
	{
		return null;
	}
	public int getIdOrder() {
		return idOrder;
	}
	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}
	public int getBill_idBill() {
		return bill_idBill;
	}
	public void setBill_idBill(int bill_idBill) {
		this.bill_idBill = bill_idBill;
	}
	public void setIdBill(int idBill) {
		// TODO Auto-generated method stub
		this.bill_idBill = bill_idBill;
	}
	
}
