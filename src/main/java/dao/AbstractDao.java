package dao;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Client;
/*****
 * aceasta clasa contine o serie de metode generice ce pot fi utilizate pentru realizarea operatiilor de inserare,stergere
 * interogare,actualizare a tabelelor din cadrul bazei de date relationale.
 * *******/
public class AbstractDao<T> {
	private final Class<T> type;
	public AbstractDao()
	{
		this.type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	private String createSelectQuery(String field)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field);
		return sb.toString();
	}
	
	public List<T> findAll()
	{
		List<T> all = new ArrayList<>();
		Connection con = ConnectionFactory.getConnection();
		Statement st=null;
		ResultSet rs=null;
		try {
			st=con.createStatement();
			rs=st.executeQuery("SELECT * FROM " + type.getSimpleName());
			all = createObjects(rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return all;
	}
	
	public T findById(int id)
	{
		Connection con = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String query = createSelectQuery("id");
		try{
			con = ConnectionFactory.getConnection();
			st = con.prepareStatement(query);
			st.setInt(1, id);
			rs = st.executeQuery();
			return createObjects(rs).get(0);
		}catch(SQLException e)
		{
			e.printStackTrace();
		}finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(st);
			ConnectionFactory.close(con);
		}
		return null;
	}
	public List<T> createObjects(ResultSet resultSet){
		List<T> list = new ArrayList<T>();
		try{
			while(resultSet.next()){
				T instance = type.newInstance();
				for(Field f:type.getDeclaredFields()){
					Object val = resultSet.getObject(f.getName());
					PropertyDescriptor descriptor = new PropertyDescriptor(f.getName(), type);
					Method method = descriptor.getWriteMethod();
					method.invoke(instance,val);
				}
				list.add(instance);}
		}catch(InstantiationException e)
		{
			e.printStackTrace();
		}catch(IllegalAccessException e)
		{
			e.printStackTrace();
		}catch(SecurityException e)
		{
			e.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	public T insert(T t)
	{
		Connection connection = ConnectionFactory.getConnection();
		Statement st = null;
		try {
			st = connection.createStatement();
			String query = "";
			query += "INSERT INTO " + t.getClass().getSimpleName() +"(";
			int i = 0;
			for(Field f:t.getClass().getDeclaredFields()){
				f.setAccessible(true);
				if(i != 0 && (f.get(t) instanceof Client) == false && (f.get(t) instanceof ArrayList) == false)
				{
					query += f.getName() + ",";
				}
				i++;
			}
			query = query.substring(0,query.length() - 1);
			query += ") values(";
			i=0;
			for(Field f:t.getClass().getDeclaredFields()){
				
				f.setAccessible(true);
				if(i != 0 && (f.get(t) instanceof Client) == false && (f.get(t) instanceof ArrayList) == false)
				{
					query += "\'" + f.get(t) +"\'" + ",";
				}
				i++;
			}
			query = query.substring(0,query.length() -1);
			query += ")";
			System.out.println(query);
			st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return t;
	}
	public T update(T t)
	{
		Connection con = ConnectionFactory.getConnection();
		Statement st = null;
		try {
			st=con.createStatement();
			String query="";
			query += "UPDATE " + t.getClass().getSimpleName() + " SET ";
			Field firstField=t.getClass().getDeclaredFields()[0];
			
			
			for(Field f:t.getClass().getDeclaredFields())
			{
				f.setAccessible(true);
				if(f != firstField && (f.get(t) instanceof Client) == false && (f.get(t) instanceof ArrayList) == false)
				{
					query += f.getName() + "=" + "\'" + f.get(t) + "\'" + ",";
				}
			}
			query = query.substring(0,query.length()-1);
			firstField.setAccessible(true);
			query += " WHERE " + firstField.getName() + "=" + "\'" + firstField.get(t) + "\'";
			System.out.println(query);
			st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return t;
	}
	public T delete(T t)
	{
		Connection con = ConnectionFactory.getConnection();
		Statement st = null;
		try {
			st=con.createStatement();
			String query = "";
			query += "DELETE FROM " + t.getClass().getSimpleName() + " WHERE ";
			Field firstField=null;
			firstField=t.getClass().getDeclaredFields()[0];
			firstField.setAccessible(true);
			query += firstField.getName() + "=" + "\'" + firstField.get(t) + "\'";
			System.out.println(query);
			st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return t;
	}
	
}