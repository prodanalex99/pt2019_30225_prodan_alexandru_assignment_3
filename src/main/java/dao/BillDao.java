package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Bill;
import model.Client;
import model.Order;

public class BillDao extends AbstractDao<Bill> {
	/***
	 * metoda de inserare a unei noi facturi in tabelul Bill
	 * ****/
	/*
	public Bill insert(Bill c)
	{
		Connection con = ConnectionFactory.getConnection();
		try {
			String query = "INSERT INTO Bill(totalOrdersPrice,Client_id) values(?,?)";
			PreparedStatement st = con.prepareStatement(query);
			st.setFloat(1, c.getTotalOrdersPrice());
			st.setInt(2, c.getClient().getId());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return c;
	}
	*/
	/****
	 * metoda care returneaza id-ul ultimei chitante din tabelul Bill
	 * ***/
	public int lastBillId(){
		Connection con = ConnectionFactory.getConnection();
		int max = -1;
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT MAX(idBill) FROM Bill");
			
			while(rs.next())
			{
				max = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return max;
	}
	/******
	 * metoda care actualizeaza o anumita chitanta data ca si argument
	 * *******/
	public Bill update(Bill b)
	{
		Connection con = ConnectionFactory.getConnection();
		String query = "UPDATE Bill SET totalOrdersPrice=? where idBill=?";
		try {
			PreparedStatement st = con.prepareStatement(query);
			st.setFloat(1, b.getTotalOrdersPrice());
			st.setInt(2, b.getIdBill());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return b;
	}
	/******
	 * metoda care sterge o anumita chitanta data ca si argument
	 * *******/
	/*
	public Bill delete(Bill b)
	{
		Connection con = ConnectionFactory.getConnection();
		String query = "DELETE from Bill where idBill=?";
		
		try {
			PreparedStatement st = null;
			st = con.prepareStatement(query);
			st.setInt(1, b.getIdBill());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return b;
		
	}
	*/
}
