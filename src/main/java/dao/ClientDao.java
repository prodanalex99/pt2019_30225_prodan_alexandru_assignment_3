package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Client;

public class ClientDao extends AbstractDao<Client> {
	/*****
	 * metoda ce returneza toti clientii din tabelul Client
	 * ****/
	public List<Client> findAll()
	{
		Connection con = ConnectionFactory.getConnection();
		List<Client> clients = new ArrayList<Client>();
		try {
			Statement st = con.createStatement();
			String query = "SELECT * from Client";
			ResultSet rs = st.executeQuery(query);
			while(rs.next())
			{
				Client client = new Client(rs.getInt("id"),rs.getString("firstName"),rs.getString("lastName"),rs.getString("email"),rs.getString("address"),rs.getString("iban"));
				clients.add(client);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clients;
	}
	/***
	 * metoda de inserare a unui nou client in tabelul Client
	 * ***/
	/*
	public Client insert(Client c)
	{
		Connection con = ConnectionFactory.getConnection();
		try {
			String query = "INSERT INTO Client(firstName,lastName,email,address,iban) values(?,?,?,?,?)";
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, c.getFirstName());
			st.setString(2, c.getLastName());
			st.setString(3, c.getEmail());
			st.setString(4, c.getAddress());
			st.setString(5, c.getIban());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
	}
	*/
	/***
	 * metoda care actualizeaza un anumit client dat ca si argument din tabelul Client
	 * ***/
	
	/*
	public Client update(Client c)
	{
		Connection con = ConnectionFactory.getConnection();
		
		//TODO preluare id
		//Ca idee voi pastra in interfata o variabila globala care va retine id-ul clinetului selectat din
		//tabel
		String query = "UPDATE Client SET firstName=?,lastName=?,email=?,address=?,iban=? where id=?";
		try {
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, c.getFirstName());
			st.setString(2, c.getLastName());
			st.setString(3, c.getEmail());
			st.setString(4, c.getAddress());
			st.setString(5, c.getIban());
			st.setInt(6,c.getId());//id
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
	}
	*/
	/*****
	 * metoda care sterge un anumit client dat ca si argument din tabelul Client
	 * ****/
	/*
	public Client delete(Client c)
	{
		Connection con = ConnectionFactory.getConnection();
		try {
			Statement st = con.createStatement();
			String query = "DELETE FROM Client where id="+c.getId();
			int rowsAffected = st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c;
	}
	*/
}
