package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Product;


public class ProductDao extends AbstractDao<Product> {
	/**
	 * metoda ce returneaza o lista cu toate produsele din tabelul Product
	 * ***/
	public List<Product> findAll()
	{
		Connection con = ConnectionFactory.getConnection();
		List<Product> products = new ArrayList<Product>();
		try {
			Statement st = con.createStatement();
			String query = "SELECT * FROM Product";
			ResultSet rs = st.executeQuery(query);
			while(rs.next())
			{
				Product product = new Product(rs.getInt("idProduct"),rs.getString("NameOfProduct"), rs.getFloat("Price"), rs.getInt("Quantity"));
				products.add(product);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return products;
	}
	/**
	 * metoda de inserare a unui nou produs dat ca si argument in tabelul Product
	 * ***/
	/*
	public Product insert(Product c)
	{
		Connection con = ConnectionFactory.getConnection();
		try {
			PreparedStatement st = con.prepareStatement("INSERT INTO Product(NameOfProduct,Price,Quantity) values (?,?,?)");
			st.setString(1, c.getNameOfProduct());
			st.setFloat(2, c.getPrice());
			st.setFloat(3, c.getQuantity());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
	}
	*/
	/**
	 * metoda de actualizare a unui nou produs dat ca si argument in tabelul Product
	 * ***/
	/*
	public Product update(Product c)
	{
		Connection con = ConnectionFactory.getConnection();
		try {
			PreparedStatement st = con.prepareStatement("UPDATE Product SET NameOfProduct=?,Price=?,Quantity=? where idProduct=?");
			st.setString(1, c.getNameOfProduct());
			st.setFloat(2, c.getPrice());
			st.setFloat(3, c.getQuantity());
			st.setInt(4, c.getId());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c;
	}
	*/
	/**
	 * metoda de stergere a unui nou produs dat ca si argument in tabelul Product
	 * ***/
	/*
	public Product delete(Product p)
	{
		Connection con = ConnectionFactory.getConnection();
		try {
			Statement st = con.createStatement();
			String query = "DELETE FROM Product where idProduct="+p.getId();
			int rowsAffected = st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return p;
		
	}
	*/
	/***
	 * metoda de cautare a unui anumit produs dat ca si argument
	 * ***/
	public Product find(Product p)
	{
		Connection con = ConnectionFactory.getConnection();
		Statement st = null;
		ResultSet rs = null;
		Product newProduct = new Product(p.getNameOfProduct(),p.getPrice(),p.getQuantity());
		try {
			st=con.createStatement();
			rs=st.executeQuery("SELECT * from Product where idProduct=" + p.getIdProduct());
			while(rs.next())
			{
				newProduct.setQuantity(rs.getInt(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return newProduct;
	}
}
