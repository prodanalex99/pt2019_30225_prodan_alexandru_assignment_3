package presentation;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ProductMenu extends JFrame {
	/*****
	 * fielduri ce algatuiesc acest jframe
	 * *****/
	private JTable table;
	private JPanel panel;
	private JButton updateButton = new JButton("Update Product");
	private JButton deleteButton = new JButton("Delete Product");
	JScrollPane scrollPane;
	/******
	 * constructor ce initializeaza interfata
	 * *****/
	public ProductMenu()
	{
		super("ProductMenu");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(640, 480);
		
		panel = new JPanel();
		//panel.setLayout(null);
		
		table = new JTable(new DefaultTableModel(new Object[]{"Id","Name Of Product","Price","Quantity"},0));
		table.setPreferredScrollableViewportSize(new Dimension(500,300));
		table.setFillsViewportHeight(true);
		scrollPane=new JScrollPane(table);
		panel.add(scrollPane);
		panel.add(updateButton);
		panel.add(deleteButton);
		
		this.add(panel);
		this.setResizable(false);
		
		this.setVisible(true);
	}
	/******
	 * metode gettere si settere
	 * *****/
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JButton getUpdateButton() {
		return updateButton;
	}

	public void setUpdateButton(JButton updateButton) {
		this.updateButton = updateButton;
	}

	public JButton getDeleteButton() {
		return deleteButton;
	}

	public void setDeleteButton(JButton deleteButton) {
		this.deleteButton = deleteButton;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
	/*****
	 * metode ce adauga actionListeneri
	 * ****/
	public void addDeleteListener(ActionListener listener)
	{
		deleteButton.addActionListener(listener);
	}
	public void addUpdateListener(ActionListener listener)
	{
		updateButton.addActionListener(listener);
	}
}
