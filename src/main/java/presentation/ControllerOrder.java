package presentation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import businesslogic.BillBll;
import businesslogic.ClientBll;
import businesslogic.OrderBll;
import businesslogic.ProductBll;
import model.Bill;
import model.Client;
import model.Order;
import model.Product;
/*****
 * Clasa care face legatura dintre elementele din interfata referitoare la adaugarea unui nou ordin si nivelul businesslogic
 * referitor la tabelul Order
 * ******/
public class ControllerOrder {
	private OrderMenu orderMenu;
	private OrderBll orderBll;
	private ProductBll productBll;
	private ClientBll clientBll;
	private BillBll billBll;
	private Client selectedClient;
	private Bill bill;
	private ArrayList<Product> products = new ArrayList<>();
	private ArrayList<Order> orders = new ArrayList<>();
	private  boolean confirmOrder = false;
	public ControllerOrder(OrderMenu orderMenu,OrderBll orderBll,ProductBll productBll,ClientBll clientBll,BillBll billBll){
		this.orderMenu = orderMenu;
		this.orderBll = orderBll;
		this.productBll = productBll;
		this.clientBll = clientBll;
		this.billBll = billBll;
		getClientData();
		getProductData();
		orderMenu.addInsertClientListener(new InsertClientListener());
		orderMenu.addInsertProductListener(new InsertProductListener());
		orderMenu.addGenerateBillListener(new GenerateBillListener());
		orderMenu.addOrderListener(new OrderListener());
		new Timer().scheduleAtFixedRate(new TimerTask() {
			public void run() {
				checkingBill();
			}
		}, 0,2);
	}
	public void resetTable(){
		for(Order order:orders){
			for(Product product:order.getProducts()){
				product.setQuantity(product.getQuantity() + productBll.find(product).getQuantity());
				productBll.updateProduct(product);
			}
		}
		
	}
	public void checkingBill(){
		if(orderMenu.isClosing() == true && confirmOrder == false && products.size() != 0){
			Bill deletedBill = new Bill();
			deletedBill.setIdBill(billBll.lastBillId());
			billBll.deleteBill(deletedBill);
			orderMenu.setClosing(false);
			//orderMenu.setClosing(false);
			resetTable();
		}
	}
	public void getProductData(){
		List<Product> products = productBll.findAll();
		DefaultTableModel model = (DefaultTableModel) orderMenu.getProductsTable().getModel();
		for(Product p:products){
			System.out.println(p.getIdProduct());
			model.addRow(new Object[]{p.getIdProduct(),p.getNameOfProduct(),p.getPrice(),p.getQuantity()});
		}
		orderMenu.getProductsTable().setModel(model);
	}
	
	public void getClientData()
	{
		List<Client> clients = clientBll.findAll();
		DefaultTableModel model = (DefaultTableModel) orderMenu.getClientTable().getModel();
		for(Client c:clients){
			System.out.println(c.getId());
			model.addRow(new Object[]{c.getId(),c.getFirstName(),c.getLastName(),c.getEmail(),c.getAddress(),c.getIban()});
		}
		orderMenu.getClientTable().setModel(model);
	}
	class InsertClientListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			DefaultTableModel model = (DefaultTableModel)orderMenu.getClientTable().getModel();
			int row = orderMenu.getClientTable().getSelectedRow();
			Client c = new Client(Integer.parseInt(model.getValueAt(row, 0).toString()), model.getValueAt(row,1).toString(), model.getValueAt(row, 2).toString(), model.getValueAt(row,3).toString(),model.getValueAt(row, 4).toString(), model.getValueAt(row, 5).toString());
			selectedClient = c;
			model.removeRow(row);
			orderMenu.getBtnAddClient().setEnabled(false);
			//creem bill-ul cand inseram clientul
			bill = new Bill(c, orders, 0);
			billBll.insert(bill); //momentan avem un total de zero urmand sa fie updatat
			//last id
			int lastId = billBll.lastBillId();
			bill.setIdBill(lastId);
		}
	}
	class InsertProductListener implements ActionListener{
		public void refreshProductTable(List<Product> newList){
			DefaultTableModel model = (DefaultTableModel) orderMenu.getProductsTable().getModel();
			model.setRowCount(0);
			for(Product p:newList){
				System.out.println(p.getIdProduct());
				model.addRow(new Object[]{p.getIdProduct(),p.getNameOfProduct(),p.getPrice(),p.getQuantity()});
			}
			orderMenu.getProductsTable().setModel(model);
		}
		public void actionPerformed(ActionEvent e) {
			DefaultTableModel model = (DefaultTableModel)orderMenu.getProductsTable().getModel();
			int row = orderMenu.getProductsTable().getSelectedRow();
			int quantity = Integer.parseInt(orderMenu.getQuantity().getText());
			if(Integer.parseInt(model.getValueAt(row, 3).toString()) - quantity >= 0){
				products.add(new Product(Integer.parseInt(model.getValueAt(row, 0).toString()),model.getValueAt(row, 1).toString(),Float.parseFloat(model.getValueAt(row, 2).toString()),quantity));
				productBll.updateProduct(new Product(Integer.parseInt(model.getValueAt(row, 0).toString()),model.getValueAt(row,1).toString(),Float.parseFloat(model.getValueAt(row, 2).toString()),Integer.parseInt(model.getValueAt(row, 3).toString()) - quantity));
				List<Product> newList = productBll.findAll();
				refreshProductTable(newList);
			}else{
				JOptionPane.showMessageDialog(null, "UnderStock product!");
			}}}
	class GenerateBillListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Document document = new Document();
			try {
				PdfWriter.getInstance(document, new FileOutputStream(selectedClient.getFirstName() + selectedClient.getLastName() + "Bill.pdf"));
				document.open();
				Font font = FontFactory.getFont(FontFactory.COURIER,20,BaseColor.BLACK);
				document.add(new Paragraph(selectedClient.getFirstName() + " " + selectedClient.getLastName() + "Bill" + "\n",font));
				int i = 1;
				float total = 0;
				for(Order order:orders){
					document.add(new Paragraph("Order" + i +"\n",font));
					for(Product p:order.getProducts()){
						document.add(new Paragraph(p.toString() + "\n",font));
					}
					document.add(new Paragraph("Total price="+order.getTotalPrice()+"\n",font));
					total += order.getTotalPrice();
					i++;
				}
				document.add(new Paragraph("Total price for all orders=" + total + "\n",font));
				document.close();
				JOptionPane.showMessageDialog(null,"Bill generated!");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}}}
	class OrderListener implements ActionListener{
		public float calculateTotalOrderPrice(ArrayList<Product> products){
			float total = 0;
			for(Product p:products){
				total += p.getPrice() * p.getQuantity();
			}
			System.out.println("Total=" + total);
			bill.setTotalOrdersPrice(total);
			return total;
		}
		public void actionPerformed(ActionEvent e) {
			confirmOrder = true;
			Order order = new Order();
			order.setProducts(products);
			order.setTotalPrice(calculateTotalOrderPrice(products));
			order.setIdBill(bill.getIdBill());
			orderBll.insert(order);
			billBll.updateBill(bill);
			orders.add(order);
			for(Product p:products){
				System.out.println(p.getNameOfProduct() + "price=" + p.getPrice() + " quantity=" + p.getQuantity());
			}
			products = new ArrayList<>();
			JOptionPane.showMessageDialog(null, "New Order added!");
		}
		
	}
}
