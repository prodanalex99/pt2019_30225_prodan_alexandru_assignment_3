package presentation;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Menu extends JFrame {
	/****
	 * elementele care alcatuiesc principala interfata grafica
	 * ***/
	private JComboBox options = new JComboBox(new String[]{"AddClient","Client Menu","AddProduct","ProductMenu","Add order"});
	private JPanel panel = new JPanel();
	private JButton btnChoose = new JButton("Ok");
	
	/***
	 * constructor utilizat pentru initializare
	 * ****/
	public Menu(){
		super("Menu");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(600,400);
		this.setResizable(false);
		this.setVisible(true);
		
		this.setLocationRelativeTo(null);
		panel.setLayout(null);
		
		options.setBounds(250,200,100,30);
		btnChoose.setBounds(250,250,100,30);
		panel.add(options);
		panel.add(btnChoose);
		this.add(panel);
	}
	/****
	 * metode gettere si settere
	 * ***/
	
	public JComboBox getOptions() {
		return options;
	}
	public void setOptions(JComboBox options) {
		this.options = options;
	}
	public JPanel getPanel() {
		return panel;
	}
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	public JButton getBtnChoose() {
		return btnChoose;
	}
	public void setBtnChoose(JButton btnChoose) {
		this.btnChoose = btnChoose;
	}
	/****
	 * metoda ce adauga un actionListener butonului de alegere a operatiei
	 * ***/
	public void addChooseListener(ActionListener listener)
	{
		btnChoose.addActionListener(listener);
	}
}
