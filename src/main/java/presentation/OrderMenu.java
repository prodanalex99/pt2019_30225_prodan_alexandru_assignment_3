package presentation;
import java.awt.Dimension;
import java.awt.Desktop.Action;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.omg.PortableInterceptor.ACTIVE;

import connection.ConnectionFactory;

public class OrderMenu extends JFrame {
	/*****
	 * fielduri ce algatuiesc acest jframe
	 * *****/
	private JTable clientTable;
	private JTable productsTable;
	private JScrollPane scrollPaneClient;
	private JScrollPane scrollPaneProducts;
	private JPanel panel;
	private JButton btnAddClient = new JButton("Add Client");
	private JButton btnAddProduct = new JButton("Add Product");
	private JButton btnAddOrder = new JButton("Add order");
	private JButton btnGenerateBill = new JButton("Generate Bill");
	private JLabel lblQuantity = new JLabel("Quantity:");
	private JTextField quantity = new JTextField();
	private boolean isClosing = false;
	/******
	 * constructor ce initializeaza interfata
	 * *****/
	public OrderMenu(){
		super("OrderMenu");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(1256,800);
		panel = new JPanel();
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event)
			{
				if (JOptionPane.showConfirmDialog(OrderMenu.this, 
			            "Are you sure you want to close this window?", "Close Window?", 
			            JOptionPane.YES_NO_OPTION,
			            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
				{
					isClosing = true;
				}
			}
		});
		//panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setLayout(null);
		clientTable = new JTable(new DefaultTableModel(new Object[]{"Id","FirstName","LastName","Email","Address","Iban"},0));
		clientTable.setPreferredScrollableViewportSize(new Dimension(500, 300));
		clientTable.setFillsViewportHeight(true);
		scrollPaneClient = new JScrollPane(clientTable);
		scrollPaneClient.setBounds(100,200,500,300);
		
		productsTable = new JTable(new DefaultTableModel(new Object[]{"Id","Name Of Product","Price","Quantity"},0));
		productsTable.setPreferredScrollableViewportSize(new Dimension(500, 300));
		productsTable.setFillsViewportHeight(true);
		
		scrollPaneProducts = new JScrollPane(productsTable);
		
		scrollPaneProducts.setBounds(600, 200, 500, 300);
		
		
		btnAddClient.setBounds(100,520,100,50);
		btnAddProduct.setBounds(600,520,150,50);
		btnAddOrder.setBounds(100,570,100,50);
		btnGenerateBill.setBounds(600,570,150,50);
		lblQuantity.setBounds(800,570,50,25);
		quantity.setBounds(850,570,150,25);
		panel.add(scrollPaneClient);
		panel.add(scrollPaneProducts);
		panel.add(btnAddClient);
		panel.add(btnAddProduct);
		panel.add(btnAddOrder);
		panel.add(btnGenerateBill);
		panel.add(lblQuantity);
		panel.add(quantity);
		this.add(panel);
		this.setResizable(false);
		this.setVisible(true);
		
	}
	/******
	 * metode gettere si settere
	 * *****/
	public JTable getClientTable() {
		return clientTable;
	}

	public void setClientTable(JTable clientTable) {
		this.clientTable = clientTable;
	}

	public JTable getProductsTable() {
		return productsTable;
	}

	public void setProductsTable(JTable productsTable) {
		this.productsTable = productsTable;
	}

	public JScrollPane getScrollPaneClient() {
		return scrollPaneClient;
	}

	public void setScrollPaneClient(JScrollPane scrollPaneClient) {
		this.scrollPaneClient = scrollPaneClient;
	}

	public JScrollPane getScrollPaneProducts() {
		return scrollPaneProducts;
	}

	public void setScrollPaneProducts(JScrollPane scrollPaneProducts) {
		this.scrollPaneProducts = scrollPaneProducts;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JButton getBtnAddClient() {
		return btnAddClient;
	}

	public void setBtnAddClient(JButton btnAddClient) {
		this.btnAddClient = btnAddClient;
	}

	public JButton getBtnAddProduct() {
		return btnAddProduct;
	}

	public void setBtnAddProduct(JButton btnAddProduct) {
		this.btnAddProduct = btnAddProduct;
	}

	public JButton getBtnAddOrder() {
		return btnAddOrder;
	}

	public void setBtnAddOrder(JButton btnAddOrder) {
		this.btnAddOrder = btnAddOrder;
	}

	public JButton getBtnGenerateBill() {
		return btnGenerateBill;
	}

	public void setBtnGenerateBill(JButton btnGenerateBill) {
		this.btnGenerateBill = btnGenerateBill;
	}
	
	/*****
	 * metode ce adauga actionListeneri
	 * ****/
	public void addInsertClientListener(ActionListener listener)
	{
		btnAddClient.addActionListener(listener);
	}
	public void addInsertProductListener(ActionListener listener)
	{
		btnAddProduct.addActionListener(listener);
	}
	
	public void addOrderListener(ActionListener listener)
	{
		btnAddOrder.addActionListener(listener);
	}
	public void addGenerateBillListener(ActionListener listener)
	{
		btnGenerateBill.addActionListener(listener);
	}

	public JTextField getQuantity() {
		return quantity;
	}

	public void setQuantity(JTextField quantity) {
		this.quantity = quantity;
	}

	public boolean isClosing() {
		return isClosing;
	}

	public void setClosing(boolean isClosing) {
		this.isClosing = isClosing;
	}
}
