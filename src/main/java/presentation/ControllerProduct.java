package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import businesslogic.ProductBll;
import model.Product;

public class ControllerProduct {
	
	/***
	 * cele 2 interfete referitoare la operatiile pe produse
	 * ***/
	private ProductAdd productAdd;
	private ProductMenu productMenu;
	/****
	 * business logic pentru produs
	 * ****/
	private ProductBll productBll;
	
	/****
	 * constructori ai clasei ControllerProduct
	 * ****/
	public ControllerProduct(ProductAdd productAdd,ProductBll productBll)
	{
		this.productAdd = productAdd;
		this.productBll = productBll;
		productAdd.addProductListener(new AddListener());
	}
	public ControllerProduct(ProductMenu productMenu,ProductBll productBll)
	{
		this.productMenu = productMenu;
		this.productBll = productBll;
		List<Product> products = productBll.findAll();
		DefaultTableModel model = (DefaultTableModel) productMenu.getTable().getModel();
		for(Product p:products)
		{
			System.out.println(p.getIdProduct());
			model.addRow(new Object[]{p.getIdProduct(),p.getNameOfProduct(),p.getPrice(),p.getQuantity()});
		}
		productMenu.getTable().setModel(model);
		
		productMenu.addDeleteListener(new DeleteListener());
		productMenu.addUpdateListener(new UpdateListener());
	}
	
	
	/***
	 * actionListener pentru adaugarea unui nou produs
	 * ****/
	class AddListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Product p = new Product(productAdd.getTxtName().getText(), Float.parseFloat(productAdd.getTxtPrice().getText()), Integer.parseInt(productAdd.getTxtQuantity().getText()));
			productBll.insertProduct(p);
			JOptionPane.showMessageDialog(null, "New Product Added!");
		}
		
	}
	/*****
	 * action listnere pentru stergerea unui produs
	 * ****/
	class DeleteListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			DefaultTableModel model =(DefaultTableModel)productMenu.getTable().getModel();
			int row = productMenu.getTable().getSelectedRow();
			Product p = new Product(Integer.parseInt(model.getValueAt(row, 0).toString()),model.getValueAt(row, 1).toString(),Float.parseFloat(model.getValueAt(row, 2).toString()),Integer.parseInt(model.getValueAt(row, 3).toString()));
			productBll.deleteProduct(p);
			model.removeRow(row);
			JOptionPane.showMessageDialog(null, "Deletion successful!");
		}
		
	}
	/****
	 * actionlistener pentru actualizarea unui produs
	 * *****/
	class UpdateListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			DefaultTableModel model = (DefaultTableModel)productMenu.getTable().getModel();
			int row = productMenu.getTable().getSelectedRow();
			productBll.updateProduct(new Product(Integer.parseInt(model.getValueAt(row, 0).toString()),model.getValueAt(row, 1).toString(),Float.parseFloat(model.getValueAt(row, 2).toString()),Integer.parseInt(model.getValueAt(row, 3).toString())));
			JOptionPane.showMessageDialog(null, "Update succesfull!");
		}
		
	}
}
