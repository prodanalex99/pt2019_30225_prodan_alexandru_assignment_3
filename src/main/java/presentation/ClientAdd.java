package presentation;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ClientAdd extends JFrame{
	
	/**
	 * variabilele instanta ce reprezinta elementele ce alcatuiesc acest jframe jLabeluri,JTextFielduri,jpanel si jbutton
	 * ****/
	private JLabel lbFirstName = new JLabel("First Name:");
	private JLabel lbLastName = new JLabel("Last Name:");
	private JLabel lbAddress = new JLabel("Address:");
	private JLabel lbEmail = new JLabel("Email:");
	private JLabel lbIban = new JLabel("Iban:");
	
	
	private JTextField tfFirstName = new JTextField();
	private JTextField tfLastName = new JTextField();
	private JTextField tfAddress = new JTextField();
	private JTextField tfEmail = new JTextField();
	private JTextField tfIban = new JTextField();
	
	
	private JButton btnAdd = new JButton("Add");
	private JPanel panel = new JPanel();
	/***
	 * constructorul clasei
	 * ****/
	public ClientAdd()
	{
		
		super("Client");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(600,400);
		this.setResizable(false);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		panel.setLayout(null);
		
		lbFirstName.setBounds(100, 100,90 , 20);
		lbLastName.setBounds(100, 120,  90, 20);
		lbEmail.setBounds(100,140 , 90, 20);
		lbAddress.setBounds(100,160 , 90, 20);
		lbIban.setBounds(100, 180, 90, 20);
		
		tfFirstName.setBounds(170,100,150,20);
		tfLastName.setBounds(170,120,150,20);
		tfEmail.setBounds(170,140,150,20);
		tfAddress.setBounds(170,160,150,20);
		tfIban.setBounds(170,180,150,20);
		btnAdd.setBounds(170,220,150,20);
		panel.add(lbFirstName);
		panel.add(lbLastName);
		panel.add(lbEmail);
		panel.add(lbAddress);
		panel.add(lbIban);
		panel.add(tfFirstName);
		panel.add(tfLastName);
		panel.add(tfEmail);
		panel.add(tfAddress);
		panel.add(tfIban);
		panel.add(btnAdd);
		this.add(panel);
		
	}
	/***
	 * metode gettere si settere
	 * ***/
	public JLabel getLbFirstName() {
		return lbFirstName;
	}

	public void setLbFirstName(JLabel lbFirstName) {
		this.lbFirstName = lbFirstName;
	}

	public JLabel getLbLastName() {
		return lbLastName;
	}

	public void setLbLastName(JLabel lbLastName) {
		this.lbLastName = lbLastName;
	}

	public JLabel getLbAddress() {
		return lbAddress;
	}

	public void setLbAddress(JLabel lbAddress) {
		this.lbAddress = lbAddress;
	}

	public JLabel getLbEmail() {
		return lbEmail;
	}

	public void setLbEmail(JLabel lbEmail) {
		this.lbEmail = lbEmail;
	}

	public JLabel getLbIban() {
		return lbIban;
	}

	public void setLbIban(JLabel lbIban) {
		this.lbIban = lbIban;
	}

	public JTextField getTfFirstName() {
		return tfFirstName;
	}

	public void setTfFirstName(JTextField tfFirstName) {
		this.tfFirstName = tfFirstName;
	}

	public JTextField getTfLastName() {
		return tfLastName;
	}

	public void setTfLastName(JTextField tfLastName) {
		this.tfLastName = tfLastName;
	}

	public JTextField getTfAddress() {
		return tfAddress;
	}

	public void setTfAddress(JTextField tfAddress) {
		this.tfAddress = tfAddress;
	}

	public JTextField getTfEmail() {
		return tfEmail;
	}

	public void setTfEmail(JTextField tfEmail) {
		this.tfEmail = tfEmail;
	}

	public JTextField getTfIban() {
		return tfIban;
	}

	public void setTfIban(JTextField tfIban) {
		this.tfIban = tfIban;
	}

	public JButton getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(JButton btnAdd) {
		this.btnAdd = btnAdd;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	/****
	 * metoda care adauga un actionListener butonului de add
	 * ***/
	public void addClientListener(ActionListener listener)
	{
		btnAdd.addActionListener(listener);
	}
}
