package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.lang.model.type.ArrayType;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businesslogic.ClientBll;
import model.Client;
import model.Product;

public class ControllerClient {
	/***
	 * cele 2 interfere grafice pentru client
	 * ***/
	private ClientAdd clientAdd;
	private ClientMenu clientMenu;
	/*****
	 * business logic pentru client
	 * ****/
	private ClientBll clientBll;
	
	/****
	 * constructori pentru controllerClient
	 * ****/
	public ControllerClient(ClientAdd clientAdd,ClientBll clientBll)
	{
		this.clientAdd = clientAdd;
		this.clientBll = clientBll;
		clientAdd.addClientListener(new AddListener());
	}
	public ControllerClient(ClientMenu clientMenu,ClientBll clientBll)
	{
		this.clientMenu = clientMenu;
		this.clientBll = clientBll;
		List<Client> clients = clientBll.findAll();
		DefaultTableModel model = (DefaultTableModel) clientMenu.getTable().getModel();
		for(Client c:clients)
		{
			System.out.println(c.getId());
			model.addRow(new Object[]{c.getId(),c.getFirstName(),c.getLastName(),c.getEmail(),c.getAddress(),c.getIban()});
		}
		clientMenu.getTable().setModel(model);
		
		clientMenu.addDeleteListener(new DeleteListener());
		clientMenu.addUpdateListener(new UpdateListener());
	}
	
	/***
	 * clasa interna ce reprezinta un listener pentru butonul de adaugare a unui nou client
	 * *****/
	class AddListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println(clientAdd.getTfFirstName().getText());
			Client c = new Client(clientAdd.getTfFirstName().getText(),clientAdd.getTfLastName().getText(),clientAdd.getTfEmail().getText(),clientAdd.getTfAddress().getText(),clientAdd.getTfIban().getText());
			clientBll.validateClient(c);
			clientBll.insertClient(c);
			JOptionPane.showMessageDialog(null, "New Client Added!");
		}
	}
	/***
	 * clasa interna ce reprezinta un listener pentru butonul de stergere a unui  client
	 * *****/
	class DeleteListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			DefaultTableModel model = (DefaultTableModel) clientMenu.getTable().getModel();
			int row = clientMenu.getTable().getSelectedRow();
			Client c = new Client(Integer.parseInt(model.getValueAt(row, 0).toString()),model.getValueAt(row, 1).toString(), model.getValueAt(row, 2).toString(),model.getValueAt(row, 3).toString(),model.getValueAt(row, 4).toString(),model.getValueAt(row, 5).toString());
			clientBll.deleteClient(c);
			model.removeRow(row);
			JOptionPane.showMessageDialog(null, "Deletion successful!");
		}
		
	}
	/***
	 * clasa interna ce reprezinta un listener pentru butonul de actualizare a unui  client
	 * *****/
	class UpdateListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			DefaultTableModel model = (DefaultTableModel) clientMenu.getTable().getModel();
			int row = clientMenu.getTable().getSelectedRow();
			clientBll.UpdateClient(new Client((Integer)model.getValueAt(row,0),(String)model.getValueAt(row, 1),(String)model.getValueAt(row, 2),(String)model.getValueAt(row, 3),(String)model.getValueAt(row, 4),(String)model.getValueAt(row, 5)));
			JOptionPane.showMessageDialog(null, "Update succesfull!");
		}
		
	}
}
