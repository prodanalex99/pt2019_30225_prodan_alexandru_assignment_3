package presentation;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class TableGenerator {
	/***
	 * metoda ce genereaza un tabel ce are ca si coloane atributele tipurilor de obiecte primite si populeaza tabelul cu elementele
	 * primite
	 * ****/
	public JTable createTable(List<Object> objects){
		Object[] columnNames = new Object[1000];
		int i = 0;
		for(Field f:objects.get(0).getClass().getDeclaredFields()){
			columnNames[i] = new Object();
			columnNames[i] = f.getName();
			i++;
		}
		DefaultTableModel model = new DefaultTableModel(columnNames,0);
		JTable table = new JTable(model);
		for(Object o:objects){
			Object[] row = new Object[1000];
			int i1 = 0;
			for(Field f:o.getClass().getDeclaredFields()){
				f.setAccessible(true);
				try {
					row[i1] = f.get(o);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				i1++;
			}
			model.addRow(row);
		}
		table.setModel(model);
		return table;
	}
}
