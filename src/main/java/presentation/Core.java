package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.RepaintManager;

import businesslogic.BillBll;
import businesslogic.ClientBll;
import businesslogic.OrderBll;
import businesslogic.ProductBll;


//oare nu poti face mai multe controllere ? unul pentru client,unul pentru  Order,unul pentru Bill?
//oare trebuie pentru bill ?
public class Core {
	/***
	 * Elementele din nivelul de business logic al aplicatiei si interfata principala a aplicatiei
	 * **/
	private Menu menu;
	private ClientBll clientBll;
	private ProductBll productBll;
	private OrderBll orderBll;
	private BillBll billBll;
	/****
	 * constructorul clasei
	 * ***/
	public Core(Menu menu,ClientBll clientBll,ProductBll productBll,OrderBll orderBll,BillBll billBll)
	{
		this.menu = menu;
		this.clientBll = clientBll;
		this.productBll = productBll;
		this.orderBll = orderBll;
		this.billBll = billBll;
		menu.addChooseListener(new ChooseListener());
		
	}
	
	/****
	 * listener pentru alegerea clientului
	 * ***/
	class ChooseListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			int option = menu.getOptions().getSelectedIndex();
			//System.out.println(option);
			switch(option)
			{
			case 0:
				ClientAdd clientAdd = new ClientAdd();
				clientAdd.setVisible(true);
				ControllerClient controller = new ControllerClient(clientAdd, clientBll);
				break;
			case 1:
				ClientMenu clientMenu = new ClientMenu();
				clientMenu.setVisible(true);
				ControllerClient controller1 = new ControllerClient(clientMenu, clientBll);
				break;
			case 2:
				ProductAdd productAdd = new ProductAdd();
				productAdd.setVisible(true);
				ControllerProduct controller2 = new ControllerProduct(productAdd, productBll);
				break;
			case 3:
				ProductMenu productMenu = new ProductMenu();
				productMenu.setVisible(true);
				ControllerProduct controller3 = new ControllerProduct(productMenu, productBll);
				break;
			case 4:
				OrderMenu orderMenu = new OrderMenu();
				orderMenu.setVisible(true);
				ControllerOrder controller4 = new ControllerOrder(orderMenu, orderBll, productBll, clientBll, billBll);
			default:
				break;
			}
		}
		
	}
}
