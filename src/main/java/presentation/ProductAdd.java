package presentation;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ProductAdd extends JFrame {
	/*****
	 * fielduri ce algatuiesc acest jframe
	 * *****/
	private JLabel lblName = new JLabel("Name of Product");
	private JLabel lblPrice = new JLabel("Price");
	private JLabel lblQuantity = new JLabel("Quantity");
	
	private JTextField txtName = new JTextField();
	private JTextField txtPrice = new JTextField();
	private JTextField txtQuantity = new JTextField();
	private JButton btnAdd = new JButton("Add");
	private JPanel panel = new JPanel();
	/******
	 * constructor ce initializeaza interfata
	 * *****/
	public ProductAdd()
	{
		super("Product");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(600,400);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		panel.setLayout(null);
		lblName.setBounds(100, 100,200 , 20);
		lblPrice.setBounds(100, 120,  90, 20);
		lblQuantity.setBounds(100,140 , 90, 20);
		
		txtName.setBounds(200,100,150,20);
		txtPrice.setBounds(200,120,150,20);
		txtQuantity.setBounds(200,140,150,20);
		btnAdd.setBounds(200,170,150,20);
		panel.add(lblName);
		panel.add(lblPrice);
		panel.add(lblQuantity);
		panel.add(txtName);
		panel.add(txtPrice);
		panel.add(txtQuantity);
		panel.add(btnAdd);
		this.add(panel);
		this.setVisible(true);
	}
	/******
	 * metode gettere si settere
	 * *****/
	public JLabel getLblName() {
		return lblName;
	}
	public void setLblName(JLabel lblName) {
		this.lblName = lblName;
	}
	public JLabel getLblPrice() {
		return lblPrice;
	}
	public void setLblPrice(JLabel lblPrice) {
		this.lblPrice = lblPrice;
	}
	public JLabel getLblQuantity() {
		return lblQuantity;
	}
	public void setLblQuantity(JLabel lblQuantity) {
		this.lblQuantity = lblQuantity;
	}
	public JTextField getTxtName() {
		return txtName;
	}
	public void setTxtName(JTextField txtName) {
		this.txtName = txtName;
	}
	public JTextField getTxtPrice() {
		return txtPrice;
	}
	public void setTxtPrice(JTextField txtPrice) {
		this.txtPrice = txtPrice;
	}
	public JTextField getTxtQuantity() {
		return txtQuantity;
	}
	public void setTxtQuantity(JTextField txtQuantity) {
		this.txtQuantity = txtQuantity;
	}
	public JButton getBtnAdd() {
		return btnAdd;
	}
	public void setBtnAdd(JButton btnAdd) {
		this.btnAdd = btnAdd;
	}
	public JPanel getPanel() {
		return panel;
	}
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	/*****
	 * metode ce adauga actionListeneri
	 * ****/
	public void addProductListener(ActionListener listener)
	{
		btnAdd.addActionListener(listener);
	}
}
