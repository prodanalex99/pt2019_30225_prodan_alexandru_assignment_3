package businesslogic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import connection.ConnectionFactory;
import dao.OrderDao;
import model.Order;

public class OrderBll {
	/***
	 * variabila instanta prin intermediul careia se manipuleaza datele din tabelul Order
	 * ******/;
	private OrderDao orderDao;
	/**
	 * constructorul clasei
	 * **/
	public OrderBll()
	{
		orderDao = new OrderDao();
	}
	
	/***
	 * metoda de inserare a unui nou ordin de plata dat ca si argument in tabelul Order
	 * ***/
	public Order insert(Order o)
	{
		Connection con = ConnectionFactory.getConnection();
		String query = "INSERT INTO Order(totalPrice,Bill_idBill) values(?,?)";
		try {
			PreparedStatement st = con.prepareStatement(query);
			st.setFloat(1, o.getTotalPrice());
			st.setInt(2, o.getBill_idBill());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return o;
	}
}
