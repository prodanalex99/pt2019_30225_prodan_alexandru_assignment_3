package businesslogic;

import java.lang.instrument.IllegalClassFormatException;
import java.util.List;
import java.util.NoSuchElementException;

import dao.ProductDao;
import model.Product;

public class ProductBll {
	/***
	 * variabila instanta prin intermediul careia se manipuleaza datele din tabelul Product
	 * ******/;
	private ProductDao productDao;
	/**
	 * constructorul clasei
	 * ***/
	public ProductBll()
	{
		productDao = new ProductDao();
	}
	/**
	 * metoda de inserare a unui nou produs dat ca si argument in tabelul Product
	 * ***/
	public Product insertProduct(Product p)
	{
		Product product = productDao.insert(p);
		if(product == null)
		{
			throw new NoSuchElementException("Product insertion failed!");
		}
		return product;
	}
	/**
	 * metoda ce returneaza o lista cu toate produsele din tabelul Product
	 * ***/
	public List<Product> findAll()
	{
		List<Product> products = productDao.findAll();
		if(products == null)
		{
			throw new NoSuchElementException("Product view failed!");
		}
		return products;
	}
	/**
	 * metoda de stergere a unui nou produs dat ca si argument in tabelul Product
	 * ***/
	public Product deleteProduct(Product p)
	{
		Product product = productDao.delete(p);
		if(product == null)
		{
			throw new IllegalArgumentException("Product deletion failed!");
		}
		return product;
	}
	/**
	 * metoda de actualizare a unui nou produs dat ca si argument in tabelul Product
	 * ***/
	public Product updateProduct(Product p)
	{
		Product product = productDao.update(p);
		if(product == null)
		{
			throw new IllegalArgumentException("Product update failed!");
		}
		return product;
	}
	/***
	 * metoda de cautare a unui anumit produs dat ca si argument
	 * ***/
	public Product find(Product p)
	{
		Product product = productDao.find(p);
		if(product == null)
		{
			throw new IllegalArgumentException("Product not found!");
		}
		return product;
	}
}
