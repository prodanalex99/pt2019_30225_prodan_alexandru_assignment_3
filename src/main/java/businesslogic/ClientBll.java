package businesslogic;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import dao.ClientDao;
import model.Client;
import businesslogic.validators.*;
public class ClientBll {
	/*****
	 * lista de validatori ai clasei
	 * *******/
	private List<Validator<Client>> validators
	/***
	 * variabila instanta prin intermediul careia se manipuleaza datele din tabelul Client
	 * ******/;
	private ClientDao clientDao;
	
	/**
	 * constructorul clasei
	 * **/
	public ClientBll()
	{
		clientDao = new ClientDao();
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new IbanValidator());
	}
	/***
	 * metoda de validare a unui client dat ca si argument
	 * */
	public void validateClient(Client c)
	{
		for(Validator<Client> validator:validators)
		{
			validator.validate(c);
		}
	}
	/****
	 * metoda de cautare a unui client dupa id
	 * ****/
	public Client findClientById(int id)
	{
		Client c = clientDao.findById(id);
		validateClient(c);
		if(c == null){
			throw new NoSuchElementException("Student with id=" + id + " was not found!");
		}
		return c;
	}
	/***
	 * metoda de inserare a unui nou client in tabelul Client
	 * ***/
	public Client insertClient(Client c)
	{
		Client client = clientDao.insert(c);
		if(client == null)
		{
			throw new NoSuchElementException("Client cannot be inserted!");
		}
		return client;
	}
	/*****
	 * metoda ce returneza toti clientii din tabelul Client
	 * ****/
	public List<Client> findAll()
	{
		List<Client> clients= clientDao.findAll();
		
		if(clients == null)
		{
			throw new NoSuchElementException("Client view failed!");
		}
		return clients;
	}
	/*****
	 * metoda care sterge un anumit client dat ca si argument din tabelul Client
	 * ****/
	public Client deleteClient(Client c)
	{
		Client client = clientDao.delete(c);
		if(client == null)
		{
			throw new IllegalArgumentException("Client deletion failed!");
		}
		return client;
	}
	/***
	 * metoda care actualizeaza un anumit client dat ca si argument din tabelul Client
	 * ***/
	public Client UpdateClient(Client c)
	{
		Client client = clientDao.update(c);
		if(client == null)
		{
			throw new IllegalArgumentException("Client update failed!");
		}
		return client;
	}
}
