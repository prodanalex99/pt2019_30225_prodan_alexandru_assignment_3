package businesslogic;

import dao.BillDao;
import model.Bill;

public class BillBll {
	/***
	 * variabila instanta prin intermediul careia se manipuleaza datele din tabelul Bill
	 * ******/
	private BillDao billDao;
	public BillBll()
	{
		billDao = new BillDao();
	}
	/**
	 * metoda care insereaza o noua chitanta in cadrul aplicatiei
	 * ***/
	public Bill insert(Bill b)
	{
		Bill bill = billDao.insert(b);
		if(bill == null)
		{
			throw new IllegalArgumentException("Cannot add a new Bill!");
		}
		return bill;
	}
	/****
	 * metoda care returneaza id-ul ultimei chitante din tabelul Bill
	 * ***/
	public int lastBillId()
	{
		int id = billDao.lastBillId();
		if(id == -1)
		{
			throw new IllegalArgumentException("Error getting last bill id!");
		}
		return id;
	}
	
	/******
	 * metoda care actualizeaza o anumita chitanta data ca si argument
	 * *******/
	public Bill updateBill(Bill b)
	{
		Bill bill = billDao.update(b);
		
		if(bill == null)
		{
			throw new IllegalArgumentException("Bill update failed!");
		}
		return bill;
	}
	/******
	 * metoda care sterge o anumita chitanta data ca si argument
	 * *******/
	public Bill deleteBill(Bill b)
	{
		Bill bill = billDao.delete(b);
		if(bill == null)
		{
			throw new IllegalArgumentException("Cannot delete the bill!");
		}
		return bill;
	}
}
