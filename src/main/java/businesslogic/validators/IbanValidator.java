package businesslogic.validators;

import java.util.regex.Pattern;

import model.Client;

public class IbanValidator implements Validator<Client> {
	/*****
	 * patternul ce prezinta regulile dupa care se valideaza ibanul
	 * ****/
	private String pattern = "[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}";
	/*******
	 * metoda validate suprascrisa din interfata Validator
	 * ******/
	public void validate(Client t) {
		Pattern myPattern = Pattern.compile(pattern);
		if(myPattern.matcher(t.getIban()).matches() == false)
		{
			throw new IllegalArgumentException("Invalid Iban");
		}
	}
	
}
