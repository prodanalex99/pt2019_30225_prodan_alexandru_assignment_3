package businesslogic.validators;


/****
 * interfata validator ce contine metoda abstracta validate
 * ******/
public interface Validator<T> {
	public void validate(T t);
}
