package businesslogic.validators;

import java.util.regex.Pattern;

import model.Client;

public class EmailValidator implements Validator<Client> {
	/*****
	 * patternul ce prezinta regulile dupa care se valideaza emailul
	 * ****/
	private static String pattern = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
	/*******
	 * metoda validate suprascrisa din interfata Validator
	 * ******/
	public void validate(Client t) {
		Pattern myPattern = Pattern.compile(pattern);
		if(myPattern.matcher(t.getEmail()).matches()== false)
		{
			throw new IllegalArgumentException("Email is invalid!");
		}
	}
	
}
