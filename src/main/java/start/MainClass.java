package start;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import businesslogic.BillBll;
import businesslogic.ClientBll;
import businesslogic.OrderBll;
import businesslogic.ProductBll;
import connection.ConnectionFactory;
import dao.AbstractDao;
import dao.ClientDao;
import model.Client;
import presentation.Core;
import presentation.Menu;
import presentation.TableGenerator;
/******
 * Clasa main din interiorul careia incepe executia oricarui program
 * ****/
public class MainClass {
	public static void main(String[] args) {
		
		
		Menu menu = new Menu();
		menu.setVisible(true);
		ClientBll clientBll = new ClientBll();
		ProductBll productBll = new ProductBll();
		OrderBll orderBll = new OrderBll();
		BillBll billBll = new BillBll();
		
		
		Core controller = new Core(menu, clientBll,productBll,orderBll,billBll);
		ConnectionFactory.getConnection();
		
		
		//AbstractDao<Client> client = new AbstractDao<>();
		//client.insert(new Client(10, "alex", "prodan", "aaa@gmail.com","adresa", "RO123456789012345"));
		
		
		
		
		
		///GenerateTable
		/*
		List<Object> clients = new ArrayList<>();
		clients.add(new Client(1,"alex","pd","aaa","aaa","aaa"));
		
		TableGenerator generator = new TableGenerator();
		JTable table =generator.createTable(clients);
		for(int i = 0; i < 6; i++)
		{
			System.out.println(table.getModel().getValueAt(0, i).toString());
		}
		*/
		
		
	}
}
