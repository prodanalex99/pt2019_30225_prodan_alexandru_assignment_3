-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP database if exists WareHouse;
CREATE DATABASE IF NOT EXISTS `WareHouse` DEFAULT CHARACTER SET utf8 ;
USE `WareHouse` ;

-- -----------------------------------------------------
-- Table `mydb`.`Client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `WareHouse`.`Client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NULL,
  `lastName` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  `iban` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `WareHouse`.`Product` (
  `idProduct` INT NOT NULL AUTO_INCREMENT,
  `NameOfProduct` VARCHAR(45) NULL,
  `Price` FLOAT NULL,
  `Quantity` INT NULL,
  PRIMARY KEY (`idProduct`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Bill`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `WareHouse`.`Bill` (
  `idBill` INT NOT NULL AUTO_INCREMENT,
  `totalOrdersPrice` FLOAT NULL,
  `Client_id` INT NULL,
  PRIMARY KEY (`idBill`),
  INDEX `fk_Bill_Person1_idx` (`Client_id` ASC) VISIBLE,
  CONSTRAINT `fk_Bill_Person1`
    FOREIGN KEY (`Client_id`)
    REFERENCES `WareHouse`.`Client` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `WareHouse`.`Order` (
  `idOrder` INT NOT NULL AUTO_INCREMENT,
  `totalPrice` FLOAT NULL,
  `Bill_idBill` INT NULL,
  PRIMARY KEY (`idOrder`),
  INDEX `fk_Order_Bill1_idx` (`Bill_idBill` ASC) VISIBLE,
  CONSTRAINT `fk_Order_Bill1`
    FOREIGN KEY (`Bill_idBill`)
    REFERENCES `WareHouse`.`Bill` (`idBill`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Order_has_Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `WareHouse`.`Order_has_Product` (
  `Order_idOrder` INT NOT NULL,
  `Product_idProduct` INT NOT NULL,
  PRIMARY KEY (`Order_idOrder`, `Product_idProduct`),
  INDEX `fk_Order_has_Product_Product1_idx` (`Product_idProduct` ASC) VISIBLE,
  INDEX `fk_Order_has_Product_Order1_idx` (`Order_idOrder` ASC) VISIBLE,
  CONSTRAINT `fk_Order_has_Product_Order1`
    FOREIGN KEY (`Order_idOrder`)
    REFERENCES `WareHouse`.`Order` (`idOrder`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_has_Product_Product1`
    FOREIGN KEY (`Product_idProduct`)
    REFERENCES `WareHouse`.`Product` (`idProduct`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
